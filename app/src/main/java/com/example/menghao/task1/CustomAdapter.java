package com.example.menghao.task1;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by menghao on 17/02/16.
 */
public class CustomAdapter extends ArrayAdapter<CustomObject> {

    public CustomAdapter(Context context, ArrayList<CustomObject> passedString) {
        super(context, 0, passedString);
    }

    public static Drawable getAssetImage(Context context, String filename) throws IOException {
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = new BufferedInputStream((assets.open("drawable/" + filename + ".png")));
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomObject o = getItem(position);

        String text = o.getText();
        int img = o.getImg();
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.custom_row, parent, false);

        TextView itemText = (TextView) customView.findViewById(R.id.item_text);
        ImageView itemImage = (ImageView) customView.findViewById(R.id.item_image);

        itemText.setText(text);
//        itemImage.setImageDrawable(getContext().getResources().getDrawable(img));
//        try {
//            itemImage.setImageDrawable(getAssetImage(getContext(), "icon"));
//        }catch (IOException e){
//            Log.e("error", "getView: ", e);
//        }
        itemImage.setImageResource(img);

        return customView;
    }
}
