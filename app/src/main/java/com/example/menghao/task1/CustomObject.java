package com.example.menghao.task1;

/**
 * Created by menghao on 17/02/16.
 */
public class CustomObject {
    public String text;
    public int img;

    CustomObject(String text, int img){
        this.text = text;
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public int getImg() {
        return img;
    }
}
