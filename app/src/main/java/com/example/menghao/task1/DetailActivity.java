package com.example.menghao.task1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_row);
        Bundle extras = getIntent().getExtras();
        TextView itemText = (TextView) findViewById(R.id.item_text);
        ImageView itemImage = (ImageView) findViewById(R.id.item_image);

        if (extras != null) {
            String text = extras.getString("text");
            int img = extras.getInt("image");
            if (text != null){
                itemText.setText(text);
            }
            itemImage.setImageDrawable(getResources().getDrawable(img));
        }

    }

}
